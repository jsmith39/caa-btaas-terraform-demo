variable "instance_count" {
  description = "The number of backend instances to deploy."
  default     = 1
  type        = number
  validation {
    condition     = var.instance_count >= 1
    error_message = "The instance count must be greater than or equal to 1."
  }
}
