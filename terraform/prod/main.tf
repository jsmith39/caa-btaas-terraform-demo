##################################################
# Configure terraform
##################################################
terraform {
	required_version = "~> 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "sandbox-eirc-admin"
  default_tags {
    tags = {
      "wvu:contact"    = "jsmith39@mail.wvu.edu"
      "wvu:zone"       = "prod"
      "wvu:portfolio"  = "admin"
      "wvu:tier"       = "app"
      "wvu:tagversion" = "20210708"
    }
  }
}

##################################################
# Gather information required for deployment.
##################################################
data "aws_ssm_parameter" "al2ami" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-arm64-gp2"
}

data "aws_vpc" "vpc" {
  default = true
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.vpc.id
}

#Look up the existing certificate as it was created by hand prior to this demo for brevity.
data "aws_acm_certificate" "btaas" {
  domain      = "btaas.wvu.edu"
  most_recent = true
}

#Look up the existing DNS zone aas it was created by hand prior ot this demo for brevity.
data "aws_route53_zone" "btaas" {
  name = "btaas.wvu.edu"
}

##################################################
# ec2 Instances
##################################################
resource "aws_instance" "web" {
  count           = var.instance_count
  ami             = data.aws_ssm_parameter.al2ami.value
  instance_type   = "m6g.medium"
  key_name        = "jsmith39-test"
  security_groups = [aws_security_group.egress.name, aws_security_group.ssh_access.name, aws_security_group.http_access.name]
  user_data       = <<-EOF
	#!/usr/bin/env bash
	yum update -y
	yum install -y httpd
	systemctl enable httpd
	systemctl start httpd
	cat <<- EOD > /var/www/html/index.html
	<!DOCTYPE html>
	<html>
		<head>
			<!-- Document Title -->
			<title>Sunrise and Sunset Calculator</title>
		</head>
		<body>
			<script>
				var images = [ '001.jpg', '002.jpg', '003.jpg', '004.jpg', '005.jpg', '006.jpg', '007.jpg', '008.jpg', '009.jpg', '010.jpg', '011.jpg' ];
				index = Math.floor(Math.random() * images.length);
				document.write('<img src="https://bow-tie-demo-2021-07-08.s3.amazonaws.com/' + images[index] + '" height=500 width=600 >'); document.close;
			</script>
		</body>
	</html>
	EOD

EOF
}


##################################################
# Security Groups
##################################################

resource "aws_security_group" "egress" {
  description = "Allow egress on any port"
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.egress.id
}

resource "aws_security_group" "ssh_access" {
  description = "Allow ssh access from the WVU campus"
}

resource "aws_security_group_rule" "ssh_access" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["157.182.0.0/16"]
  security_group_id = aws_security_group.ssh_access.id
}

resource "aws_security_group" "http_access" {
  description = "Allow http access from anywhere"
}

resource "aws_security_group_rule" "http_access" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh_access.id
}

resource "aws_security_group" "lb_front_end" {
  description = "load balancer front end"
}

resource "aws_security_group_rule" "lb_front_end_http" {
  security_group_id = aws_security_group.lb_front_end.id
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "lb_front_end_https" {
  security_group_id = aws_security_group.lb_front_end.id
  type              = "ingress"
  from_port         = "443"
  to_port           = "443"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "lb_front_end_egress" {
  security_group_id = aws_security_group.lb_front_end.id
  type              = "egress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}

##################################################
# Load Balancing
##################################################
resource "aws_lb" "btaas" {
  internal           = false
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.subnets.ids
  security_groups    = [aws_security_group.lb_front_end.id]
}

resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.btaas.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-FS-1-2-Res-2020-10"
  certificate_arn   = data.aws_acm_certificate.btaas.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.btaas.arn
  }
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.btaas.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_302"
    }
  }
}

resource "aws_lb_target_group" "btaas" {
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.vpc.id
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "btaas" {
  count            = length(aws_instance.web)
  target_group_arn = aws_lb_target_group.btaas.arn
  target_id        = aws_instance.web[count.index].id
  port             = 80
}

##################################################
# TLS Certificates
##################################################
# This certificate was created by hand prior to this demo for brevity.
#resource "aws_acm_certificate" "btaas" {
#  domain_name       = "btaas.wvu.edu"
#  validation_method = "EMAIL"
#  lifecycle {
#    create_before_destroy = true
#  }
#}

##################################################
# DNS Zone
##################################################
#This dns zone was created by hand prior to this demo for brevity.
#resource "aws_route53_zone" "btaas" {
#  name = "btaas.wvu.edu"
#}

resource "aws_route53_record" "btaas" {
  zone_id = data.aws_route53_zone.btaas.id
  name    = "btaas.wvu.edu"
  type    = "A"
  alias {
    name                   = aws_lb.btaas.dns_name
    zone_id                = aws_lb.btaas.zone_id
    evaluate_target_health = true
  }
}
